; 本程序能实现 4 位数的整数开平方取整
show    macro str                   ; 宏定义显示字符串
        lea     dx, str        
        mov     ah, 9h
        int     21h 
        endm
data    segment para
    about_me    db 0dh, 0ah, 'About me: 201583135 2017-11-8 $'
    input_tip   db 0dh, 0ah, 'Please input_tip the four number[0000~9999]: $'
    output_tip  db 0dh, 0ah, 'The result is: $'
    result      db 2 dup(0)         ; 四位数据最多开平方为两位
data    ends
ss_seg  segment stack
    dw  100 dup(0)
ss_seg  ends
code    segment para
    assume  cs:code, ss:ss_seg, ds:data
    main    proc far
        mov     ax, data         
        mov     ds, ax
        show    about_me            ; 显示个人信息        
        show    input_tip           ; 显示输入提示字符串
        mov     si, 10              ; 进制数据
        mov     cx, 4               ; 循环4次
        mov     ax, 0               ; ax存放数据
        mov     bx, 0               ; 中转寄存以及存放奇数
    input_char:                     ; 键盘输入四位数并转化为二进制数
        mul     si                  ; ax * si => {dx, ax}
        mov     bx, ax              ; 将ax数据存放到bx中，因为要使用al读入数据
        mov     ah, 1
        int     21h                 ; 读取输入字符
        sub     al, 30h             ; 将读入的ascii数据转换为对应的二进制数据
        and     ax, 00ffh           ; 保留ax的低八位字符，因为前面ah改变了
        add     ax, bx              ; 将新输入的数据与之前存放的数据相加到ax中
        loop    input_char          ; 完成输入，存放到了ax中
        push    ax                  ; 保存输入的数据
        show    output_tip          ; 显示输出提示字符串
        ; 计算开平方
        mov     dx, 0               ; 计数器
        mov     bx, 1               ; 奇数从1开始
        pop     ax                  ; 还原输入的数据
    again_sub: 
        sub     ax, bx              
        jb      ax_b_bx
        inc     dx
        add     bx, 2
        jmp     again_sub
    ax_b_bx:
        mov     ax, dx
        mov     cx, 3
        mov     si, 10
        mov     bx, 1
    num2asc: ; 把开方根转换成ascii型
        mov     dx, 0
        div     si
        add     dx, 30h             ; dx中存放着余数，由于余数这里小于10，
                                    ; 仅dl中为有效数据
        mov     [result+bx], dl     ; 将余数倒着存放到result
        dec     bx                  ; 存放位置前移
        loop    num2asc
        mov     cx, 2
        mov     bx, 0
    output_data: ; 输出根
        mov     dl, [result+bx]
        mov     ah, 2
        int     21h                 ; 显示结果
        inc     bx
        loop    output_data
        
        mov     ax, 4c00h
        int     21h
    main    endp
code    ends
    end     main