data         segment   ;数据段  
      buf    dw 0100h  ;假设变量为0100h 
      result dw 00h
data         ends

ssg          segment stack;堆栈段
             dw 257 dup(0)
ssg          ends

code         segment
             assume  cs:code,ss:ssg,ds:data 
start: mov   ax,data;取数据段段地址
       mov   ds,ax
       mov   bx,1
       mov   cx,0
circle:sub buf,bx    ;buf与bx相减
       cmp buf,0      ;减完后的buf和0作比较
       jl over       ;有符号数小于则跳转
       add bx,2      ;若大于等于则bx+2,cx+1
       inc cx
       jmp circle    ;重复循环
over:mov result ,cx
       int 20h       ;退出
code      ends         
end    start
