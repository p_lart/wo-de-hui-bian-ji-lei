;内存中存放 8 个 8 位有符号数，请按从大到小顺序排列
;即，低地址到高地址，大数到小数。
data    segment para
    buf db -1, 2, -33, 44, -55, 66, -77, 88
data    ends
stack   segment stack
    dw 100 dup(0)
stack   ends
code    segment para
    assume  cs:code, ds:data, ss:stack
    main    proc far
        ;将ds:bx指向存放数据的位置
        mov     ax, data
        mov     ds, ax
        mov     cx, 7           ;主循环仅需要7次
    loop_main:
        lea     bx, buf
        push    cx              ;cx=n, 子循环正好也要循环n次
                                ;压栈保存外循环循环次数
        mov     si, 0           ;标志是否有交换，当后面检测si==0时，说明
                                ;内部已经排序完毕
    loop_sub:
        ;取得第一个数据到ax里，将之与第二个数据比较大小
        ;ax大等于[bx]的话，保留原样
        ;ax小的话，就得将两个数据进行交换位置
        mov     al, [bx]
        cmp     al, [bx+1]
        jge     not_xchg        ;>=不交换
        ;ax小，进行交换
        ;push    cx
        ;mov     cl, 8
        ;shr     word ptr [bx], 8
        ;mov     [bx+1], al
        ;pop     cx
        ;也可以使用交换指令
        xchg    al, [bx+1]
        mov     [bx], al
        mov     si, 1           ;标志有交换
    not_xchg:
        ;第一步交换完成，需要依次进行一遍
        inc     bx
        loop    loop_sub
        ;完毕后，只完成了初步的重排，这时数据段中的情况是，高地址存放最小的数
        ;还需要继续执行，之后每次都减少一次
        cmp     si, 0
        je      done_xchg
        ; 仍有交换，还需重复      
        pop     cx              ;出栈，恢复cx
        loop    loop_main
    done_xchg:
        mov     ax, 4c00h
        int     21h
    main    endp
code     ends
    end     main