assume cs:code,ds:data,es:table
  
data segment  
    db '1975','1976','1977','1978','1979','1980','1981','1982','1983'  
    db '1984','1985','1986','1987','1988','1989','1990','1991','1992'  
    db '1993','1994','1995'  
      
    dd 16,22,382,1356,2390,8000,16000,24486,50065,97479,140417,197514  
    dd 345980,590827,803530,1183000,1843000,2759000,3753000,4649000,5937000  
      
    dw 3,7,9,13,28,38,130,220,476,778,1001,1442,2258,2793,4037,5635,8226  
    dw 11542,14430,15257,17800  
data ends  
  
table segment  
    db 21 dup ('year summ ne ?? ')   
table ends 

code segment
start:
    mov ax,data
    mov ds,ax
    mov ax,table
    mov es,ax
    mov bp,0H
    mov si,0H
    mov di,0H
    mov cx,21
lart:
    mov bx,0H
    mov ax,ds:[bx+0+di]    ;存放年份
    mov es:[bp+0],ax
    mov ax,ds:[bx+2+di] 
    mov es:[bp+2],ax
    mov ax,ds:[bx+84+di]    ;存放收入
    mov es:[bp+5],ax
    mov ax,ds:[bx+86+di]
    mov es:[bp+7],ax
    mov ax,ds:[bx+168+si]    ;存放雇员数目
    mov es:[bp+10],ax

    mov ax,es:[bp+5]   ;被除数的低16位
    mov dx,es:[bp+7]   ;被除数的高16位
    div word ptr es:[bp+10]   ;计算人均收入，商存在ax里
    mov es:[bp+13],ax   ;存放人均收入

    add bp,10H
    add di,4H
    add si,2H
    
    loop lart

    mov ax,4c00h
    int 21h
code ends
end start