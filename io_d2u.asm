;; 从键盘输入一个小写字母，转换成大写字母输出。
data	segment para
	str1 db 'Please inpur(a-z):$'
	str2 db 0dh, 0ah, 'The result is:$'
                                ; 这里的0dh,0ah数据，是换行回车对应的ascii码
data 	ends
stack	segment stack
	dw 100 dup(0)
stack	ends
code	segment
	assume cs:code, ss:stack, ds:data
	main proc far
		mov		ax, data		
		mov		ds, ax          ; 程序装入内存里的时候，系统将cs:ip，ss:sp
                                ; 指向代码段和堆栈段的其实位置，而ds，es则
                                ; 是指向PSP+0处，所以需要重新指定ds到data
		lea 	dx, str1		; 令ds:dx指向要显示的数据位置
		mov		ah, 9			
		int		21h				; 利用中断指令显示，要求显示内容被ds:dx所指
		mov 	ah, 1
		int 	21h				; 利用中断读入字符到al，并回显
								; a~z: 61h~7Ah
								; A~Z: 41h~5Ah
		sub		al, 20h			; 大小写相差20H，小写变大写
		; and al,11011111b		; 也可以实现变大写，这条命令可以使得
								; 大写也还会变大写
		push	ax              ; 将ax中的数据压栈，ax低八位中是对应的大写字母
                                ; 因为后面要用到ax这个寄存器来实现
                                ; 利用中断处理时，需要提前处理好ax中的已有数据
		lea 	dx,	str2        
		mov 	ah, 9
		int		21h             ; 利用中断处理，显示提示
		pop 	ax
		mov 	ah, 2
        mov     dl, al          
        int     21h             ; 利用中断处理，显示输出
		mov 	ax, 4c00h
		int 	21h             ; 返回DOS，主要的退出程序的方法，有三种，直接利用
                                ; int 20h结束程序，这个指令直接使用时，不会释放之前
                                ; 占用的资源
                                ; 利用程序段前缀PSP开头的int 20h，这样的执行方式，
                                ; 可以释放程序占用的存储空间。
                                ; 还有一种方式，就是利用21h号中断的4ch功能返回DOS
	main endp
code	ends
	end main
