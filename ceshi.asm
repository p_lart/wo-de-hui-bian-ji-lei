; @Date    : 2017-11-20 17:05:18
; @Author  : lart (lartpang@163.com)
; Function : test
data    segment para
    array1 dw 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
    array2 dw 10 dup(0)
    X      equ  array2 - array1
data    ends
ss_seg  segment stack
    dw  100 dup(0)
ss_seg  ends
code    segment para 
    assume  cs:code, ds:data, ss:ss_seg
    main    proc far
        mov     ax, data
        mov     ds, ax
        mov     cx, X
        mov     bx, 0
    again:
        mov     ax, word ptr [array1 + bx]
        add     ax, 4000h
        mov     word ptr [array2 + bx], ax 
        add     bx, 2
        loop    again
        mov     ax, 4c00h
        int     21h
    main    endp
code    ends
    end     main