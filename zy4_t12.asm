;内存中有两个32位有符号数，请编写完整汇编语言程序，将二者相乘，
;结果保存在64位有符号数RESULT中
data    segment para
    num_1   dd 80000002h
    num_2   dd 80005000h
    result  dw 4 dup (?)
data    ends
ss_seg  segment stack
    dw 100 dup (0)
ss_seg  ends
code    segment para
    assume  cs:code, ds:data, ss:ss_seg
    main    proc far
        mov     ax, data
        mov     ds, ax
        shl     byte ptr [num_1+3], 1
        jc      num_1_l0 
        xor     ax, ax   
        shr     byte ptr [num_1+3], 1;剥离符号位
        mov     bh, 0h
        jmp     next
    num_1_l0:;num_1<0
        mov     bh, 1h
    next:
        shl     byte ptr [num_2+3], 1
        jc      num_2_l0
        xor     ax, ax
        shr     byte ptr [num_2+3], 1
        mov     bl, 0h
        jmp     continue
    num_2_l0:;num_2<0
        mov     bl, 1h
    continue:
        ; 判断符号
        lea     si, num_1
        lea     di, num_2

        ;当做无符号输处理
        mov     ax, [si]
        mov     dx, [di]
        mul     dx
        mov     [result],ax
        mov     [result+2],dx
    
        mov     ax, [si+2]
        mov     dx, [di]
        mul     dx
        add     [result+2],ax
        adc     [result+4],dx
    
        mov     ax, [si]
        mov     dx, [di+2]
        mul     dx
        add     [result+2],ax
        adc     [result+4],dx
        adc     [result+6],0
    
        mov     ax, [si+2]
        mov     dx, [di+2]
        mul     dx
        add     [result+4],ax
        adc     [result+6],dx
        
        sub     bh, bl
        jnz     bh_nz_bl
        and     [result+7], 01111111b
        jmp     exit
    bh_nz_bl:;不同号
        or      [result+7], 10000000b
    exit:
        mov     ah, 4ch
        int     21h
    main    endp
code    ends
    end     main