;内存中从 str 开始存放一字符串，结束符为 NULL 字符，请编写程序统计该
;字符串中单词的个数
;例：string db 0dh, 0ah, ‘Hello world, welcome to DUT. CPU is central
;processing unit! ’, 0h
;统计’⋯.’中的单词个数，结果为 10
;通过检测空格来计数，当空格前为空格时不计数
data    segment para
    string  db 0dh, 0ah, 'Hello world, welcome to DUT. CPU is central '
            db 'processing unit!', 0h
    words   dw 0
data    ends
stack   segment stack
    db  256 dup(0)
stack   ends
code    segment para
    assume  cs:code, ds:data, ss:stack
    main    proc far
        mov     ax, data
        mov     ds, ax
        mov     cx, 0       ;用 cx 存放单词数
        lea     si, string
        mov     bl, ' '     ;bl 总保存当前字符的前一个字符
        cld                 ;保证右移
    load_al: 
        lodsb               ;从ds:si中以字节为单位获取数据，al只判断0以及空格
        and     al, al      ;判断 al 是否为结束符 0，al=0 zf=1，al!=0，zf=0
        jz      al_0
        cmp     al, ' '     ;比较是否是空格，只对空格计数
        jnz     bl_new
                            ;处理重复计数：计数只是根据空格的个数来计算。
                            ;重复计数主要是因为多个空格以及逗号空格连续所致。
        cmp     bl, ' '     ;比较前一个字符是否为空格，如果是则此空格
                            ;不能算一个单词，即为了防止多个空格的情况
        jz      bl_new
        inc     cx          ;只有当前字符为 ' '而且前一个字符为有
                            ;效字符时，才对单词数加 1
    bl_new: 
        mov     bl, al      ;进入这里表明此时 al 中内容不是 0 或者 ' '
                            ;符号 ,保存 al 到 bl
        jmp     load_al
    al_0:                   ;判断结束符前面是否有' '，删掉多余的计数
        cmp     bl, ' '     
        jz      done
        inc     cx          ;若结束符前是一个有效字符，那么单词数应该加 1
    done: 
        mov     words, cx
        
        mov     ax, 4c00h
        int     21h
    main    endp
code    ends
    end     main