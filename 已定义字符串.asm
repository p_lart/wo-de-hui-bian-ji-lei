;已定义的字符串，统计正负0个数，利用“字符输出”的dos功能在屏幕上显示结果

data    segment para
    str     db '-1 -8 0 9 0 23 -89 123 0', 0dh
    num     dw 4 dup (0)    ;分别存放总数，负，0，正
data    ends
ss_seg  segment stack
    dw 100 dup (0)
ss_seg  ends
code    segment para
    assume cs:code, ds:data, ss:ss_seg
    main    proc far
        mov     ax, data
        mov     ds, ax    
        ;; 开始统计
        ;; 由于输入的字符串，只能有正负数和零以及空格
        ;; 所以可以先利用空格确定数据个数
        ;; 先查找负数，用‘-’前的空格来检测统计
        ;; 再查找零，以0前的空格来检测
        ;; 正数个数可以用总数减去负数和零的数量来获得
        lea     si, str
        call    stastic_space
        mov     ax, [num]
        sub     ax, [num+2]
        sub     ax, [num+4]
        mov     [num+6], ax
        mov     ax, 4c00h
        int     21h
    main    endp

    ; 用 cx 存放数量
    ; bl 总保存当前字符的前一个字符
    ; ds:si中以字节为单位获取数据
    stastic_space   proc
        mov     cx, 0
        lea     si, str
        mov     bl, ' '     
        cld                 
        ;保证右移
    load_al: 
        lodsb
        cmp     al, 0dh   
        ;判断 al 是否为回车——回车表示字符串结束
        jz      al_0dh
        cmp     al, ' '     
        ;比较是否是空格，只对空格计数
        jnz     bl_new

    al_douhao:              
        ;al存放的是数据
        cmp     bl, ' '     
        jz      bl_new
        inc     [num] 
        jmp     bl_new

    bl_new: 
        mov     bl, al      
        ;进入这里表明此时 al 中内容是数字，保存到 bl
        cmp     al, '-'
        jz      al_neg  ;al中存放的是负号
        cmp     al, '0'
        jz      al_0
        jmp     load_al
    al_neg:;al为负
        inc     [num+2]
        jmp     load_al
    al_0:;al为0
        inc     [num+4]
        jmp     load_al
    al_0dh:                   
        ;判断结束符前面是否有' '，删掉多余的计数
        cmp     bl, ' '     
        jz      done
        inc     [num]          ;若结束符前是一个有效字符，那么数应该加1
    done: 
        ret
    stastic_space   endp
code    ends
    end     main