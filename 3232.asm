assume cs:code

code segment
    data1 dw 1000H,0000H
    data2 dw 1000H,1000H
start:
    lea si,data1
    lea di,data2
    mov ax,code
    mov ds,ax
    mov ax,[si]
    add [di],ax
    mov ax,[si+2]
    adc [di+2],ax

    mov ah,4ch
    int 21h

code ends
end start
