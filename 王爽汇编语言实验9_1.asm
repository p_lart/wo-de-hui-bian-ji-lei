;; ds:[si] 段超越实现指定段偏移，也会忽略默认的偏移段，直接按照指定的来
;; 利用段超越，可以直接为显示缓冲区指定段基址
;;
;;     mov ax,0B800H
;;     mov es,ax
;;     mov di,0000H
;;     mov es:[di],...
;;
;; 汇编中，书写数字的时候，要时刻记得加上’H'后缀，不然默认按十进制读取，以字母开头的时候，要注意前置0，否则会无法识别

assume cs:code,ds:data,ss:stack

    data segment
        db 41H,0CAH,42H,0CAH,43H,0CAH,44H,0CAH,45H,0CAH,46H,0CAH,00H
    data ends

    stack segment
        dw ?
    stack ends

    code segment
start:
    mov ax,data
    mov ds,ax
    mov ax,stack
    mov ss,ax
    mov sp,2
    mov si,0000H
    mov di,0000H
    mov cx,0001H
ifcnz:
    mov cl,[si]
    inc si
    pusH  ds
    mov ax,0B800H
    mov ds,ax
    mov [di],cl
    inc di
    inc cl
    pop ds
    loop ifcnz

    mov ax,4c00H
    int 21H
   code ends
end start