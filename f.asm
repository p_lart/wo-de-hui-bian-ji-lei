; @Date    : 2017-11-16 19:50:19
; @Author  : lart (lartpang@163.com)
; 将一个 8 位压缩 BCD 码转换为二进制数
data    segment para
    BCD_num     dd 5678h
    bin_num     db 4 dup(0)
    loop_num    db 2
data    ends
ss_seg  segment stack
    dw      100 dup(0)
ss_seg  ends
code    segment para 
    assume  cs:code, ds:data, ss:ss_seg
    main    proc far
        mov     ax, data
        mov     ds, ax
        mov     si, 10
        mov     ax, 0
        mov     cx, 0404h
    again_mul:
        mul     si
        rol     word ptr [BCD_num], cl
        mov     bx, word ptr [BCD_num] ;还是word ptr [BCD_num]
        and     bx, 000fh
        add     ax, bx
        dec     ch
        jnz     again_mul
        mov     word ptr [bin_num], ax
        mov     ax, 4c00h
        int     21h
    main    endp
code    ends
    end     main
