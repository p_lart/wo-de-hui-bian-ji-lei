; @Date    : 2017-11-16 19:50:19
; @Author  : lart (lartpang@163.com)
; 将一个 8 位压缩 BCD 码转换为二进制数
data    segment para
    BCD_num     dd 12345678h
    bin_num     db 4 dup(0)
    loop_num    db 2
data    ends
ss_seg  segment stack
    dw      100 dup(0)
ss_seg  ends
code    segment para 
    assume  cs:code, ds:data, ss:ss_seg
    main    proc far
        mov     ax, data
        mov     ds, ax
        mov     si, 10
        mov     ax, 0
        mov     dl, 2   ;分两段
        mov     di, 2
    again_loop:
        mov     cx, 0404h
    again_mul:
        mul     si
        rol     word ptr [BCD_num+di], cl
        mov     bx, word ptr [BCD_num+di]
        and     bx, 000fh
        add     ax, bx
        dec     ch
        jnz     again_mul
        mov     word ptr [bin_num+di], ax
        xor     di, di
        xor     ax, ax
        sub     [loop_num], 1
        jnz     again_loop
        ; 高四位对应的十进制乘以10，加上低四位对应的十进制
        mov     ax, word ptr [bin_num+2]
        mov     bx, 10000
        mul     bx
        add     ax, word ptr [bin_num]
        adc     dx, 0
        mov     word ptr [bin_num], ax
        mov     word ptr [bin_num+2], dx
        mov     ax, 4c00h
        int     21h
    main    endp
code    ends
    end     main
