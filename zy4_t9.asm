;将一个 16 位的无符号数 var, 转换为非压缩格式 BCD 码，存放在内存中 buf
;开始的单元中。（按高位在前、低位在后的顺序存放）
;二进制数 0FFFFH，十进制 65535，非压缩BCD 6 5 5 3 5 各一个字节的低四位
;二进制转换十进制：((0*2 + B15)*2 + B14)*2 + ? + )*2 + B0
data    segment para
    buf     db 5 dup(0)
    var     dw 0FFFH    
data    ends
stack   segment stack
    dw  100 dup(0)
stack   ends
code    segment para 
    assume  cs:code, ds:data, ss:stack
    main    proc far
        mov     ax, data
        mov     ds, ax
        mov     cx, 16          ;要进行十六次移位，利用adc获得
    loop_wai: 
        shl     var, 1          ;得到 var 的 Bi 位
        mov     bx, 4
        push    cx
        mov     cx, 5
    loop_nei:                   
        mov     al, [bx]        ;执行 buf*2 + Bi 操作
        adc     al, al
        aaa                     ;非压缩格式 BCD 码调整
        mov     [bx], al
        dec     bx
        loop    loop_nei        ;内循环为 5 次
        pop     cx
        loop    loop_wai        ;外循环为 16 次
    exit: 
        mov     ax, 4c00h
        int     21h
    main    endp
code    ends
    end     main