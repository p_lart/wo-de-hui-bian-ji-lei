;以 10 进制形式显示内存中一有符号字节数据
;例：             var db 0ffH
;屏幕应显示：     The result is: -1
;思路：二进制->十进制->ascii码
data    segment para
    var_signed      db 0ffH
    str_result      db 0dh, 0ah, 'The result is: $'
    num_signed      db 4 dup(' ');因为有符号字节数，最多就是三位十进制
data    ends
stack   segment stack
    dw 100 dup(0)
stack   ends
code    segment para
    assume  cs:code, ds:data, ss:stack
    main    proc far
        mov     ax, data
        mov     ds, ax
        mov     num_signed, '+'
        cmp     var_signed, 0   ;判断 var_signed 是正数 ,还是负数
        jge     is_pos          ;>= 为正数
        mov     num_signed, '-' ;<  为负数
        neg     var_signed      ;若 var_signed 为负，则补码的相反数
    is_pos: 
        mov     al, var_signed
        mov     cx, 3
        mov     dl, 10          ;进制
        lea     bx, num_signed+3;倒着放数据
    again: 
        mov     ah, 0           ;高八位置零
        div     dl
        add     ah, 30h         ;余数 ah
        mov     [bx], ah
        dec     bx
        loop    again           ;循环 3 次,分别得到百、十、个位

;        lea     bx, num_signed
;        cmp     byte ptr [bx+1], '0'     ;去掉数字前面的零
;        jnz     done
;        mov     cl, 8
;        shl     word ptr [bx+2], cl
;        ;mov     byte ptr [bx+3], '0';

;        cmp     byte ptr [bx+1], '0'     ;去掉数字前面的零
;        jnz     done
;        mov     cl, 8
;        shl     word ptr [bx+1], cl
;        ;mov     byte ptr [bx+2], '0'
    done:
        lea     dx, str_result  ;显示 10 进制数
        mov     ah, 9
        int     21h
        ; 显示字符
        mov     cx, 4
        lea     si, num_signed
    show_char:
        mov     dl, [si]
        cmp     dl, '0'
        jz      next
        mov     ah, 2h
        int     21h 
    next:
        inc     si
        loop    show_char
         
        mov     ax, 4c00h
        int     21h
    main    endp
code    ends
    end     main