;内存中有 8 个 16 位数，请编写程序将 8 个数倒序排放
;例：定义内存中 8 个数 
;   buf dw 100, 3, 1, 20, 40, -2, 7, 10
;程序运行后， buf 开始应为： 
;   buf dw 10, 7, -2, 40, 20, 1, 3, 100
;思路：要实现倒序存放，已有思路有，利用栈，或者反复循环交换
;      明显，使用栈，更为便捷
data    segment para
    buf dw -1, 2, -33, 44, -555, 666, -7777, 8888
data    ends
stack   segment stack
    dw 100 dup(0)
stack   ends
code    segment para
    assume  cs:code, ds:data, ss:stack
    main    proc far
        mov     ax, data
        mov     ds, ax
        lea     bx, buf
        mov     cx, 8
    stack_push:
        push    [bx]
        add     bx, 2
        loop    stack_push
        lea     bx, buf
        mov     cx, 8
    stack_pop:
        pop     [bx]
        add     bx, 2
        loop    stack_pop
        mov     ax, 4c00h
        int     21h
    main    endp
code    ends
    end     main