assume cs:code,ds:data
    
    data segment
        db 41H,0CAH,42H,0CAH,43H,0CAH,44H,0CAH,45H,0CAH,46H,0CAH
        db 00H,00H
    data ends
    
    code segment
start:
    mov ax,data
    mov ds,ax
    mov ax,0B800H
    mov es,ax

    mov si,0000H
    mov di,0000H
    mov cx,0006H
ifcnz:
    mov ax,ds:[si]
    mov es:[di],ax
    add si,2
    add di,2

    ;; if(cx-1 == 0){exit_loop} else {loop}
    loop ifcnz 

    mov ax,4c00H
    int 21H
   code ends
end start