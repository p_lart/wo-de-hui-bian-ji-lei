;数据段从 100H 开始存放字符串 str_first ，从 200H 开始存放 str_second ，二者均以
;NULL 字符为结束符，编写程序将 str_second 拷贝到 str_first 末尾，形成一个完整字
;符串
;例： 
;   ORG 100H
;   str_first db 0dh, 0ah, ‘Hello ’, 0
;   ORG 200H
;   str_second db ‘Automation! ’, 0
;程序运行后结果应为：
;   str_first db 0dh, 0ah, ‘Hello Automation! ’, 0
data    segment para
    ORG     100H
    str_first   db 0dh, 0ah, 'Hello ', 0
    ORG     200H
    str_second  db 'Automation! ', 0
    count       equ ($-str_second)
data    ends
ss_seg  segment stack
    dw 100 dup(0)
ss_seg  ends
code    segment para
    assume  cs:code, ds:data, ss:ss_seg
    main    proc far
        mov     ax, data
        mov     ds, ax
        mov     es, ax
        lea     di, str_first   ;es:di 指向str_first首地址
        mov     al, 0
        repnz   scasb           ;查找到str_first结束符NULL，最后di会多加一次
        dec     di              ;让es:di指向该位置
        lea     si, str_second  ;ds:si指向str_second首地址
        cld                     ;DF置零，右移
;    again:                     ;可以使用ladsb配合stosb外加检查al
;        lodsb                   ;AL (ds:si), si = si + 1
;        stosb                   ;(es:di) AL, di = di + 1
;        cmp     al, 0           ;判断是否到 str_second 的结束符
;        jnz     again
        mov     cx, count
        rep     movsb           ;重复搬移
        lea     si, str_first   ;ds:si 指向拷贝后的 str_first 首地址
    display: 
        lodsb                   ;显示拷贝后的 str_first 字符串
        cmp     al, 0
        jz      exit            ;显示完毕
        call    pchar
        jmp     display
    exit: 
        mov     ax, 4c00h
        int     21h
    main    endp

    ;功能：显示单个字符
    ;输入参数：AL中存放ASCII
    ;输出参数：无
    pchar   proc
        mov     dl, al
        mov     ah, 2
        int     21h
        ret
    pchar   endp
code    ends
    end     main