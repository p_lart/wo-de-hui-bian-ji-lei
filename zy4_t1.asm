code    segment para
    assume  cs:code, ds:code, ss:code
    org     0100h
    main    proc near
    again:
        lea     dx, str_dis
        mov     ah, 9
        int     21h             ;显示初始字符串，提示输入
        mov     ah, 1
        int     21h             ;获得输入字符，存到al里
        cmp     al, 'A'         ;判断输入字符是否是A~Z的字符
        jb      error
        cmp     al, 'Z'
        ja      error           ;保证输入合法
        add     al, 20h         ;大写+20h=小写
        mov     str_num, ax     ;将ax保存
        lea     dx, str_res
        mov     ah, 9
        int     21h             ;显示结果提示字符串
        mov     ax, str_num     ;将ax恢复
        mov     dl, al
        mov     ah, 2
        int     21h             ;显示最终结果
        mov     ax, 4c00h
        int     21h
    error:
        lea     dx, str_err
        mov     ah, 9
        int     21h
        jmp     again
    main    endp

    str_num dw 0000h
    str_dis db 0dh, 0ah, 'Please input(A~Z):$'
    str_res db 0dh, 0ah, 'The result is:$'
    str_err db 0dh, 0ah, 'The data of input is invalid!$'
code    ends
    end     main
