;;功能：将内存中存放的8个16位有符号数求和，存放到
;;      内存变量SUM中
data    segment para
    buf dw -1, 2, -33, 44, -555, 666, -7777, 8888
    sum dd 0
data    ends
ss_seg  segment stack
    dw  100 dup(0)
ss_seg  ends
code    segment para
    assume  cs:code, ds:data, ss:ss_seg
        main    proc far
        mov     ax, data
        mov     ds, ax              ;指定数据位置
        lea     bx, buf             ;bx指向buf首地址
        mov     cx, 8               ;指定循环次数
    w2d:
        mov     ax, [bx]
        cwd                         ;有符号数字扩展为双字，默认使用{dx,ax}
        add     word ptr sum, ax    ;32 位数相加
        adc     word ptr sum + 2, dx
        inc     bx
        inc     bx
        loop    w2d                 ;循环 8 次
        mov     ax, 4c00h
        int     21h
    main    endp
code    ends
    end     main
