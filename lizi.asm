SHOW    MACRO B
        MOV AH,02H
        MOV DL,B
        INT 21H
        ENDM

DATA    SEGMENT
  DATA1   DB  32H,39H,30H,35H,34H
  DATA2   DB  33H
  RESULT  DB 6 DUP(00H)
DATA    ENDS

STACK   SEGMENT
  STA   DB    20 DUP(?)
  TOP   EQU   LENGTH STA
STACK   ENDS

CODE SEGMENT
    ASSUME  CS:CODE,DS:DATA,SS:STACK
START: MOV  AX,DATA
       MOV  DS,AX
       MOV  AX,STACK
       MOV  SS,AX
       MOV  AX,TOP
       MOV  SP,AX
       MOV  SI,OFFSET DATA1
       MOV  CL,5
       SHOW 20H
       CALL DISPL
       SHOW 0DH
       SHOW 0AH
       SHOW 2AH
       MOV  SI,OFFSET DATA2
       MOV  BL,[SI]
       AND  BL,00001111B
       MOV  CL,1
       CALL DISPL
       SHOW 0DH
       SHOW 0AH
       MOV  CL,7
S1:    SHOW 2DH
       LOOP S1
       SHOW 0DH
       SHOW 0AH
       MOV  SI,OFFSET DATA1
       MOV  DI,OFFSET RESULT
       MOV  CX,05
       ADD  SI,4
       ADD  DL,5
LOOP1: MOV  AL,[SI]
       AND  AL,00001111B
       DEC  SI
       MUL  BL
       AAM
       MOV  BH,AH
       MOV  AH,0
       ADD  AL,[DI]
       AAA  
       MOV  [DI],AL
       ADD  [DI-1],AH
       MOV  AL,BH
       MOV  AH,0
       ADD  AL,[DI-1]
       AAA  
       MOV  [DI-1],AL
       ADD  [DI-2],AH
       DEC  DI
       LOOP LOOP1
       MOV  CX,06
       MOV  SI,OFFSET RESULT
LOP:   MOV  DL,[SI]
       ADD  DL,30H
       MOV  [SI],DL
       INC  SI
       LOOP LOP
       MOV  SI,OFFSET RESULT
       MOV  CL,6
       CALL DISPL
       MOV  AX,4C00H
       INT  21H
  DISPL   PROC NEAR
  SD1:    SHOW [SI]
          INC SI
          LOOP SD1
          RET
  DISPL   ENDP
CODE ENDS
  END START