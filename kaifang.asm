DATA SEGMENT PARA
    MSG     DB 'PLEASE INPUT THE NUMBER',0DH,0AH,'$'
    ENTER   DB ' ',0DH,0AH,'$'
    STRING  DB 'THE RESULT IS:',0DH,0AH,'$'
DATA ENDS
SS_SEG SEGMENT STACK
    DB 100 DUP (0)
SS_SEG ENDS
CODE SEGMENT
    ASSUME CS:CODE,DS:DATA
    START: 
        MOV AX,DATA
        MOV DS,AX
        LEA DX,MSG
        MOV AH,9
        INT 21H ;显示输入提示字符串
        MOV AH,1
        INT 21H ;键盘输入
        AND AL,0FH
        MOV BX,1 ;奇数从 1 开始相减
        MOV CX,0 ;CX循环次数，即为所求平方根
    LOOP1: 
        SUB AX,BX
        INC BX
        INC BX
        INC CX
        CMP Al,0 ;AL与 0 比较
        JNAE LOOP1 ;若大于 0 则继续循环
        LEA DX,ENTER
        MOV AH,9
        INT 21H ;回车换行
        LEA DX,STRING
        MOV AH,9
        INT 21H ;显示输出提示字符串
        MOV DX,CX
        ADD DX,30H
        MOV AH,2
        INT 21H ;以十进制显示结果
        MOV AX,4C00H
        INT 21H
CODE ENDS
    END START