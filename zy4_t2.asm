;;asc2bin：将acsii转换为二进制数
;;输入参数：AL中存放需要转换的ASCII
;;输出参数：AL中存放转换后的二进制数并返回
asc2bin     proc
    sub     al, 30h         ;先都当做表示数字的acsii
    cmp     al, 9           ;与9比较，小于等于9的，直接跳转，大
                            ;于9的，仍然需要处理
    jbe     asc2bin_sub     ;跳转子程序
    sub     al, 7           ;继续减7
asc2bin_sub:
    ret                     ;返回主程序
asc2bin     endp
