;编写程序用键盘 1~7 键，模拟 7 个音符，进行键盘演奏！
;注：需要对 8253 的定时器 2 和 8255 的 PB 口进行初始化
data    segment para
    fre_low     dw  131, 147, 165, 175, 196, 220, 247, 262 ;低音 1-7 的频率
    fre_high    dw  262, 294, 330, 349, 392, 440, 494, 523 ;高音 1-7 的频率
    shift_1     equ 21h
    shift_2     equ 40h
    shift_3     equ 23h
    shift_4     equ 24h
    shift_5     equ 25h
    shift_6     equ 5Eh
    shift_7     equ 26h
    shift_8     equ 2Ah
data    ends
ss_seg  segment stack
    db  256 dup(0)
ss_seg  ends
code    segment
    assume cs:code,ds:data,ss:stack
    main proc far
        mov     ax, data        ;装载数据段寄存器 ds, 使之指向当前数据段
        mov     ds, ax
    main_1: 
        mov     ah, 0           ;利用 16 号中断，扫描键盘按键
        int     16h             ;AL 中返回按键 ASCII 码，AH 中返回按键扫描码
        cmp     al, 0dh         ;判断是否为回车键，是回车键就退出
        jz      exit
        cmp     al, '1'
        jb      main_2
        cmp     al, '7'
        ja      main_2
        and     ax, 000fh       ;说明按下的是低音 1-7，与操作将 ASCII
                                ;码转换到数字 1-7
        dec     al              ;将数值转换成以 0 开始的数字，以便从表中查找频率
                                ;值将数值乘以 2，因每个频率值是字变量
        shl     al, 1
        lea     bx, fre_low
    main_12:
        mov     si, ax
        mov     di, [bx+si]     ;从表中取频率值
        mov     bx, 5000        ;发声时间
        call    GenSound
        jmp     main_1
    main_2: 
        cmp     al, shift_1     ;判断是否按下 shift+1 键
        jnz     main_3
        mov     ax, 0
        jmp     main_11
    main_3: 
        cmp     al, shift_2     ;判断是否按下 shift+2 键
        jnz     main_4
        mov     ax, 2
        jmp     main_11
    main_4:     
        cmp     al, shift_3     ;判断是否按下 shift+3 键
        jnz     main_5
        mov     ax, 4
        jmp     main_11
    main_5:     
        cmp     al, shift_4     ;判断是否按下 shift+4 键
        jnz     main_6
        mov     ax, 6
        jmp     main_11
    main_6:     
        cmp     al, shift_5     ;判断是否按下 shift+5 键
        jnz     main_7
        mov     ax, 8
        jmp     main_11
    main_7: 
        cmp     al, shift_6     ;判断是否按下 shift+6 键
        jnz     main_8
        mov     ax, 10
        jmp     main_11
    main_8:     
        cmp     al, shift_7     ;判断是否按下 shift+7 键
        jnz     main_9
        mov     ax, 12
        jmp     main_11
    main_9: 
        cmp     al, shift_8     ;判断是否按下 shift+8 键
        jnz     main_10
        mov     ax, 14
        jmp     main_11
    main_10:    
        jmp     main_1
    main_11:    
        lea     bx, fre_high
        jmp     main_12
    exit:
        mov     4c00h
        int     21h
    main    endp

    ;发声程序
    ;入口： di 中存放的是定时器 2 的计数值
    ; bx 中存放的是延时的时间
    ;出口：无
    GenSound    proc
        push    ax              ;保护现场
        push    bx
        push    cx
        push    dx
        push    si
        push    di
        mov     al, 0b6h        ; 写定时器模式
        out     43h, al
        mov     dx, 12h         ;定时器分频 ,533h*896=123280h
        mov     ax, 3280h
        div     di              ;533h*896/ 给定频率
        out     42h, al         ;写定时器 2 的低字节
        mov     al, ah
        out     42h, al         ;写定时器 2 的高字节
        in      al, 61h         ;得到当前 61h 端口的设置 ,并保存在 ah 中
        mov     ah, al
        or      al, 03h         ;打开扬声器发声
        out     61h, al
    GenSound_2:
        mov     cx, 3000
    GenSound_1:
        nop
        loop    GenSound_1
        dec     bx
        jnz     GenSound_2
        mov     al, ah          ;关闭扬声器
        out     61h, al
        pop     di              ;恢复现场
        pop     si
        pop     dx
        pop     cx
        pop     bx
        pop     ax
        ret
    GenSound    endp
code    ends
    end     main