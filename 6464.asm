assume cs:code

code segment
    DAT1 dw 0002h, 8000h
    DAT2 dw 5000h, 1000h
    result dw 4 dup (?)
start:
    mov ax,code
    mov ds,ax
    lea si,DAT1
    lea di,DAT2
    
    mov ax,cs:[si]
    mov dx,cs:[di]
    mul dx
    mov [result],ax
    mov [result+2],dx

    mov ax,cs:[si+2]
    mov dx,cs:[di]
    mul dx
    add [result+2],ax
    adc [result+4],dx

    mov ax,cs:[si]
    mov dx,cs:[di+2]
    mul dx
    add [result+2],ax
    adc [result+4],dx
    adc [result+6],0

    mov ax,cs:[si+2]
    mov dx,cs:[di+2]
    mul dx
    add [result+4],ax
    adc [result+6],dx

    mov ah,4ch
    int 21h
code ends
end start