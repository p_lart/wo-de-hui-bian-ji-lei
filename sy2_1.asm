show_str    macro str_input
    lea     dx, str_input        
    mov     ah, 9h
    int     21h 
            endm
show_char   macro char
    mov     dl, byte ptr char
    mov     ah, 2h
    int     21h 
            endm
bin2asc     macro bin
    local   num
    cmp     byte ptr bin, 10
    jb      num
    add     byte ptr bin, 7h
num:
    add     byte ptr bin, 30h
            endm
al2bl       macro reg8
    cmp     reg8, ' '
    mov     reg8, al
    jnz     load_al
            endm
data    segment para
    about_me    db 0dh, 0ah, 'my name is Pang Youwei: 201583135 2017-11-15 $'
    input_tip   db 0dh, 0ah, 'Please input numbers: ', '$'
    str_input   db 50
                db 0h
    real_buf    db 50 dup (0)           ;存放数据
    out_tip1    db 0dh, 0ah, ' number of total    is: ', '$'
    out_tip2    db 0dh, 0ah, ' number of negative is: ', '$'
    out_tip3    db 0dh, 0ah, ' number of zero     is: ', '$'
    out_tip4    db 0dh, 0ah, ' number of positive is: ', '$'
    num         db 4 dup (0)            ;分别存放总数，负，0，正
data    ends
ss_seg  segment stack
    dw 100 dup (0)
ss_seg  ends
code    segment para
    assume cs:code, ds:data, ss:ss_seg
    main    proc far
        mov         ax, data
        mov         ds, ax    
        show_str    about_me            ; 显示个人信息        
        show_str    input_tip           ; 显示输入提示字符串
        lea         dx, str_input
        mov         ah, 0ah
        int         21h
        lea         si, real_buf
        mov         bl, ' '   
        cld                 
        ;保证右移
    load_al: 
        lodsb
        cmp     al, 0dh   
        ;判断 al 是否为回车——回车表示字符串结束
        jz      al_0dh
        cmp     al, ' '
        ;比较是否是空格，只对空格计数
        jnz     bl_new
    al_douhao:              
        ;al存放的是空格
        cmp     bl, ' '
        jz      bl_new
        inc     [num]
        ;al 空格，bl数字
        mov     bl, al
        jmp     load_al
    bl_new: 
        cmp     al, '8'
        jae     al_neg  ;al中存放的是负
    al_pos:;al为非负数
        cmp     al, '0'
        ja      al_a_0
        lodsb
        cmp     al, '0'
        jnz     al_a_0 ;al为正
        inc     [num+2] ;al = 0
        mov     bl, al
        jmp     load_al
    al_neg:;al为负
        al2bl   bl
        inc     [num+1]
        jmp     load_al
    al_a_0: ;al为正
        al2bl   bl
        inc     [num+3]
        jmp     load_al
    al_0dh:;判断结束符前面是否有' '，删掉多余的计数                   
        cmp     bl, ' '     
        jz      done
        inc     [num]          
        ;若结束符前是一个有效字符，那么数应该加1
    done:;转换为ascii码
        bin2asc      [num]
        bin2asc      [num+1]
        bin2asc      [num+2]
        bin2asc      [num+3]
        show_str      out_tip1
        show_char     [num]
        show_str      out_tip2
        show_char     [num+1]
        show_str      out_tip3
        show_char     [num+2]
        show_str      out_tip4
        show_char     [num+3]
        mov     ax, 4c00h
        int     21h
    main    endp
code    ends
    end     main